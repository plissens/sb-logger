import pandas
import math

class SunspecMapper:
    def __init__(self, mappingFile):
        self.mappings = dict()
        try:
            registers = pandas.read_excel(mappingFile)    
            print("wait")      
            for register in registers.itertuples(index=True):
                if not (register._7 == '-'):
                    if not math.isnan(register._7):
                        scaling = 0
                        if not (register._12 == '-'):
                            if not math.isnan(register._12):                            
                                scaling = register._12 -1
                            else:
                                print("not mapping scaling: " + str(register._12))
                        mapping = {
                            'address' : register._7 -1,
                            'type' : register._11,
                            'name' : register._8,
                            'scaling' : scaling,
                            'value' : ""
                            }
                        self.mappings[register._7 -1] = mapping
                    else:
                        print("not mapping address: " + str(register._7))
        except:
            print("Error initializing mappings")