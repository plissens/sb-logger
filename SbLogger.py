from pyModbusTCP.client import ModbusClient
from pyModbusTCP import utils

class SbLogger:
    def __init__(self, sunnyBoy, dataTypes):
        self.name = sunnyBoy['name']
        self.dataTypes = dataTypes
        try:
            self.sbClient = ModbusClient(host=sunnyBoy['host'], port=sunnyBoy['port'], auto_open=True, auto_close=True)
            self.sbClient.unit_id(126)
            self.sbClient.debug(False)
        except ValueError:
            print("Error with host or port params")
    def getRegister(self, mapping):
        raw = self.sbClient.read_input_registers(mapping["address"],self.dataTypes[mapping["type"]])
        match mapping["type"]:
            case "uint32":
                return str(utils.word_list_to_long(raw)[0])
            case "uint16":
                return str(raw[0])
            case "string16":
                tempstring = ""
                for bytes in raw:
                    if not bytes == 0:
                        tempstring = tempstring + bytes.to_bytes(2, byteorder='big').decode('utf-8')
                return str(tempstring.split('\x00',1)[0])
            case "string8":
                tempstring = ""
                for bytes in raw:
                    if not bytes == 0:
                        tempstring = tempstring + bytes.to_bytes(2, byteorder='big').decode('utf-8')
                return str(tempstring.split('\x00',1)[0])
            case "int16":
                return str(utils.get_2comp(raw[0]))
            case "acc64":
                return str(utils.word_list_to_long(raw, long_long=True)[0])
            case "acc32":
                return str(utils.word_list_to_long(raw)[0])
            case "uint64":
                match mapping["name"]:
                    case "MAC":
                        return str("{0:0{1}x}".format(utils.word_list_to_long(raw, long_long=True)[0],14))
                    case _:
                        return str(utils.word_list_to_long(raw, long_long=True)[0])
            case _:
                return "cannot convert type " + mapping["type"] + " yet"
