import SbLogger
import SunspecMapper
import json
import logging
import logging.handlers
import math
import time
import socket
from pythonjsonlogger import jsonlogger

sunnyBoys = {}
settings = dict()
with open("settings.json") as settingFile:
    settings = json.loads(settingFile.read())
#all loggers in zulu time
logging.Formatter.converter = time.gmtime
#corelogger
coreLogger = logging.getLogger("SunnyBoyLogger")
coreLogger.setLevel(settings['setCoreLevel'])
fh = logging.handlers.WatchedFileHandler(settings["coreLogFile"])
fh.setLevel(settings['setCoreLevel'])
formatter = jsonlogger.JsonFormatter(
    fmt='%(asctime)s %(levelname)s %(name)s %(message)s')
fh.setFormatter(formatter)
coreLogger.addHandler(fh)
#condensedlogger
condensedLogger = logging.getLogger("SunnyBoyCondensed")
condensedLogger.setLevel(logging.INFO)
cfh = logging.handlers.WatchedFileHandler(settings["condensedLogFile"])
cfh.setLevel(logging.INFO)
cformatter = jsonlogger.JsonFormatter(
    fmt='%(asctime)s %(levelname)s %(name)s %(message)s')
cfh.setFormatter(cformatter)
condensedLogger.addHandler(cfh)
#map
mapper = SunspecMapper.SunspecMapper(settings["modbus_mapping"])
for sunnyBoy in settings["sunnyBoys"]:    
    sunnyBoys[sunnyBoy["name"]] = SbLogger.SbLogger(sunnyBoy,settings["dataTypes"])
while True:
    try:        
        allExtraDict = {}
        for key in settings["condensedRegisters"]:
                allExtraDict[key] = 0
        for sunnyBoy in settings["sunnyBoys"]:
            values = mapper.mappings
            for mapping in mapper.mappings:
                values[mapping]["value"] = sunnyBoys[sunnyBoy["name"]].getRegister(mapper.mappings[mapping])
            for value in values:
                if not values[value]["scaling"] == 0:
                    values[value]["value"] = str( float(values[value]["value"]) * math.pow(10, int(values[values[value]["scaling"]]["value"]))) 
                extraDict = {   "SunnyBoy"  : sunnyBoy["name"],
                                "register"  : values[value]["name"],
                                "type"      : values[value]["type"],
                                "address"   : str(value),
                                "scaling"   : str(values[value]["scaling"])
                }
                coreLogger.info(values[value]["value"], extra=extraDict)
            extraDict = {}
            for key in settings["condensedRegisters"]:
                extraDict[key] = values[settings["condensedRegisters"][key]]["value"]
                allExtraDict[key] = allExtraDict[key] + float(values[settings["condensedRegisters"][key]]["value"])
            condensedLogger.info(sunnyBoy["name"], extra=extraDict)
        for key in settings["condensedRegisters"]:
            allExtraDict[key] = str(allExtraDict[key])
        condensedLogger.info("total", extra=allExtraDict)
        time.sleep(60)
    except socket.gaierror as e:
        coreLogger.warning("an error occured while resolving a hostname, sleeping 60s before retry")


